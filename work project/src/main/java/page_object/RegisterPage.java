package page_object;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.Random;

public class RegisterPage {
    private WebDriver driver;

    @FindBy(id = "username")
    WebElement registerUserNameField;

    @FindBy(id = "password")
    WebElement registerPasswordField;

    @FindBy(id = "withdraw_password")
    WebElement registerWithdrawPasswordField;

    @FindBy(id = "nickname")
    WebElement registerNickNameField;

    @FindBy(name = "phone_carrier")
    WebElement registerPhoneCarrierField;

    @FindBy(id = "phone_number")
    WebElement registerPhoneNumberField;

    @FindBy(id = "master_code")
    WebElement registerMasterCodeField;

    @FindBy(className = "middle")
    WebElement registerSelectBankField;

    @FindBy(id = "bank_account_name")
    WebElement registerBankAccountHolderField;

    @FindBy(id = "bank_account_number")
    WebElement registerBankAccountNumberField;

    @FindBy(css = "li[class=\"btn\"] em button")
    WebElement registerButton;

    @FindBy(css = "li[class=\"btn\"] em var")
    WebElement cancelRegisterButton;

//    @FindBy(xpath = "//*[@class=\"complete_box active\"]//h3//font[2]")
//    WebElement pendingRegisterMessage;
    @FindBy(xpath = "//*[@class=\"complete_box active\"]//h4")
    WebElement pendingRegisterMessage;








    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public void selectPhoneCarrier(WebElement carrier, String phoneCarrier){
        WebElement holder = carrier;
        Select carrierHolder = new Select(holder);
        carrierHolder.selectByVisibleText(phoneCarrier);
    }

    public void selectBankExchange(WebElement exchange, String bank ){
        WebElement exchange1 = exchange;
        Select bankDropdown = new Select(exchange1);
        bankDropdown.selectByVisibleText(bank);
    }


    public void fillInRegisterFields(
            String username,
            String password,
            String withdrawPassword,
            String nickName,
            String phoneCarrier,
            String phoneNumber,
            String masterCode,
            String selectBank,
            String accountHolder,
            String accountNumber
    ) {
        registerUserNameField.sendKeys(username);
        registerPasswordField.sendKeys(password);
        registerWithdrawPasswordField.sendKeys(withdrawPassword);
        registerNickNameField.sendKeys(nickName);
        registerPhoneNumberField.sendKeys(phoneNumber);
        registerMasterCodeField.sendKeys(masterCode);
        registerBankAccountHolderField.sendKeys(accountHolder);
        registerBankAccountNumberField.sendKeys(accountNumber);
        selectPhoneCarrier(registerPhoneCarrierField,phoneCarrier);
        selectBankExchange(registerSelectBankField,selectBank);
    }

    public void clickOnRegisterButton(){
        registerButton.click();
    }

    public void clickOnCancelRegisterButton(){
        cancelRegisterButton.click();
    }

    public boolean getRegisterMessage(){
       return  pendingRegisterMessage.isEnabled();
    }


    }