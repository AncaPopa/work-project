package register_paige;

import org.junit.Test;
import page_object.HomePage;
import page_object.RegisterPage;
import utils.BaseTestClass;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class registerPaigeTests extends BaseTestClass {

    @Test
    public void registerOnPaige() {
        HomePage homePage = new HomePage(driver);
        homePage.selectEnglishLanguage();
        RegisterPage registerPage = homePage.clickOnSignUpButton();
        assertEquals("http://staging.mnc-1.com/register", driver.getCurrentUrl());
        registerPage.fillInRegisterFields
                ("leadtest01" + System.currentTimeMillis(),"ancatest","ancatest",
                        "leadtest01" +System.currentTimeMillis(),"LGU+", "12345678913","iliemb",
                        "Post office","225566", "1255894");

        registerPage.clickOnRegisterButton();
        assertTrue(registerPage.getRegisterMessage());

    }
}
