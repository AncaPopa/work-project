package main_paige;

import org.junit.Assert;
import org.junit.Test;
import page_object.HomePage;
import utils.BaseTestClass;

import static org.junit.Assert.assertTrue;

public class mainPageTests extends BaseTestClass {

    @Test
    public void closePopUpAndChangeLanguage() {
       HomePage homePage = new HomePage(driver);
        homePage.checkPopUp1Box();
        homePage.closePopUp1();
        homePage.checkPopUp2Box();
        homePage.closePopUp2();
        homePage.checkPopUp3Box();
        homePage.closePopUp3();
        homePage.selectEnglishLanguage();

    }

    @Test
    public void loginOnPage() {
        HomePage homePage = new HomePage(driver);
        homePage.fillInLoginFields("ancasma", "ancatest");
        homePage.clickOnLoginButton();
        assertTrue(homePage.logInButtonIsPresent());
//        Assert.assertEquals("ancaSMA",  homePage.loginMessage());
        homePage.logOutFromPage();
        assertTrue(homePage.logInButtonIsPresent());

    }


}
