package page_object;

import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePage {

    private WebDriver driver;
    @FindBy(id = "check-10")
    WebElement doNoTOpenPopUpBox1;

    @FindBy(id = "close_ad-10")
    WebElement closePopUpButton1;

    @FindBy(id = "check-11")
    WebElement doNoTOpenPopUpBox2;

    @FindBy(id = "close_ad-11")
    WebElement closePopUpButton2;

    @FindBy(id = "check-7")
    WebElement doNoTOpenPopUpBox3;

    @FindBy(id = "close_ad-7")
    WebElement closePopUpButton3;

    @FindBy(className = "dropdown-selected")
    WebElement languageButton;

    @FindBy(css = "ul[class=\"dropdown-menu active\"] li:first-child")
    WebElement englishButton;

    @FindBy(name = "username")
    WebElement userNameField;

    @FindBy(name = "password")
    WebElement passwordField;

    @FindBy(xpath = "//*[@class=\"privacy\"]//var[1]")
    WebElement loginButton;

    @FindBy(xpath = "//*[@class=\"lv_1\"]/font")
    WebElement successufullyLogInMessage;

    @FindBy(className = "user-money-balance")
    WebElement cashBalance;

    @FindBy(className = "logout")
    WebElement logOutButton;

    @FindBy(xpath = "//*[@class=\"privacy\"]//var[2]")
    WebElement signUpButton;







    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public void checkPopUp1Box() {
        doNoTOpenPopUpBox1.click();
    }

    public void closePopUp1() {
        closePopUpButton1.click();
    }

    public void checkPopUp2Box() {
        doNoTOpenPopUpBox2.click();
    }

    public void closePopUp2() {
        closePopUpButton2.click();
    }

    public void checkPopUp3Box() {
        doNoTOpenPopUpBox3.click();
    }

    public void closePopUp3() {
        closePopUpButton3.click();
    }

    public void selectEnglishLanguage() {
        languageButton.click();
        englishButton.click();
    }

    public void fillInLoginFields(String username, String password) {
        userNameField.sendKeys(username);
        passwordField.sendKeys(password);
    }

    public void clickOnLoginButton() {
        loginButton.click();
    }

    public String loginMessage() {
        return successufullyLogInMessage.getText();

    }

//    public int getBalanceValue(){
//        return Integer.parseInt(cashBalance.getText());
//    }

    public void logOutFromPage() {
        logOutButton.click();
    }

    public boolean logInButtonIsPresent() {
        return loginButton.isEnabled();
    }

    public boolean logOutButtonIsPresent(){
        return logOutButton.isEnabled();

    }
    public RegisterPage  clickOnSignUpButton(){
        signUpButton.click();
        return new RegisterPage(driver);
    }


}
